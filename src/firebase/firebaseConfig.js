import  firebase  from "firebase/app";
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyCRxHPWzMPdT34VkmNTDlNIBbmMDw6Hg6c",
    authDomain: "journal-app-5ea69.firebaseapp.com",
    projectId: "journal-app-5ea69",
    storageBucket: "journal-app-5ea69.appspot.com",
    messagingSenderId: "484373547967",
    appId: "1:484373547967:web:b9f99defd82ed6f6596f0a"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export {
    db,
    googleAuthProvider,
    firebase
}