import React from 'react'
import { NotesAppBar } from './NotesAppBar'

export const NoteScreen = () => {
    return (
        <div className="notes__main-content">
            <NotesAppBar />

            <div className="notes__content">
                <input 
                   type="text"
                   placeholder="some aweosme title" 
                   className="notes__title-input"
                   autoComplete="off"
                />

                <textarea
                    placeholder="what happened today?"
                    className="note__textarea"
                >                    
                </textarea>

                <div className="notes_image">
                    <img 
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3tjpaOaDdzI4_5NdPr7n6BABd6hibDaee-A&usqp=CAU"
                        alt="note"
                    />
                </div>
            </div>
        </div>
    )
}
