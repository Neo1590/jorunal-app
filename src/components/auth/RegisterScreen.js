import React from 'react'
import { Link } from 'react-router-dom'
import {useForm} from "../../hooks/useForm";
import validator from "validator";
import { useDispatch } from 'react-redux';
import { setError, removeError} from '../../actions/ui'

export const RegisterScreen = () => {

    const dispatch = useDispatch();

    const [ formValues, handleInputChange ] = useForm({
        name: 'jose',
        email: 'gilbertguitar15@gmail.com',
        password: '123456',
        confirm: '123456'
    });

    const { name, email, password, confirm } = formValues;

    const handleregister = (e) => {
        e.preventDefault();    
        
        if ( isFormValid() ) {
            console.log('formulario correcto')
        }
    }

    const isFormValid = () => {
        
        if(name.trim().length === 0){            
            dispatch(setError('name is required'))
            return false;
        } else if ( !validator.isEmail( email )){     
            dispatch(setError('email is not valid'))
            return false;
        } else if (password !== confirm || password.length < 5) {    
            dispatch(setError('pasword sgould be at least 6 character long'))
            return false;
        }

        dispatch(removeError());
        return true;
    }

    return (
        <>
            <h3 className="auth__title">Register</h3>  

            <form onSubmit={ handleregister }>

                <div className="auth__alert-error">
                    hola mundo
                </div>

                <input 
                    type="text"
                    placeholder="Name"
                    name="name"
                    className="auth__input"
                    autoComplete="off"
                    value={name}
                    onChange={ handleInputChange }
                />

                <input 
                    type="text"
                    placeholder="Email"
                    name="email"
                    className="auth__input"
                    autoComplete="off"
                    value={email}
                    onChange={ handleInputChange }
                />

                <input 
                    type="password"
                    placeholder="Password"
                    name="password"
                    className="auth__input"
                    value={password}
                    onChange={ handleInputChange }
                />

                <input 
                    type="password"
                    placeholder="Confirm password"
                    name="confirm"
                    className="auth__input"
                    value={confirm}
                    onChange={ handleInputChange }
                />

                <button
                    type="submit"
                    className="btn btn-primary btn-block mb-5"
                    // disabled={true}
                >
                    Register
                </button>                
                
                <Link
                    to="/auth/login"
                    className="link"
                >
                    Already registered?
                </Link>
            </form>
        </>
    )
}
